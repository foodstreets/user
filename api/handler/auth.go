package handler

import (
	"net/http"

	"gitlab.com/foodstreets/user/api/entity"
	"gitlab.com/foodstreets/user/api/form"
	"gitlab.com/foodstreets/user/api/view"

	"github.com/gin-gonic/gin"
)

type authHandler struct{}

var AuthHandler IAuthHandler

type IAuthHandler interface {
	GetByAccessToken(c *gin.Context)
	Login(c *gin.Context)
	Signup(c *gin.Context)
	Verify(c *gin.Context)
	SignupFacebook(c *gin.Context)
	LoginFacebook(c *gin.Context)
	SignupApple(c *gin.Context)
	SignupGoogle(c *gin.Context)
}

func init() {
	AuthHandler = &authHandler{}
}

func (a authHandler) GetByAccessToken(c *gin.Context) {
	formAuth := form.Auth{}
	if err := formAuth.Bind(c); err != nil {
		panic(err)
		return
	}

	auth, err := entity.EntityAuth.GetByAccessToken(formAuth)
	if err != nil {
		panic(err)
		return
	}

	resp := view.PopulateUser(auth)
	c.JSON(200, gin.H{
		"data": resp,
	})
}

func (a authHandler) Login(c *gin.Context) {
	formAuth := form.Login{}
	if err := formAuth.Bind(c); err != nil {
		panic(err)
		return
	}

	user, err := entity.EntityAuth.Login(formAuth)
	if err != nil {
		panic(err)
		return
	}

	resp := view.PopulateUser(user)
	c.JSON(200, gin.H{
		"data": resp,
	})
}

func (a authHandler) Signup(c *gin.Context) {
	formAuth := form.Signup{}
	if err := formAuth.Bind(c); err != nil {
		panic(err)
		return
	}

	user, err := entity.EntityAuth.Signup(formAuth)
	if err != nil {
		panic(err)
		return
	}
	resp := view.PopulateUser(user)
	c.JSON(200, gin.H{
		"data": resp,
	})
}

func (a authHandler) Verify(c *gin.Context) {
	formVerify := form.Verify{}
	if err := formVerify.Bind(c); err != nil {
		panic(err)
		return
	}

	user, err := entity.EntityAuth.Verify(formVerify)
	if err != nil {
		panic(err)
		return
	}

	resp := view.PopulateUser(user)
	c.JSON(http.StatusOK, gin.H{
		"data": resp,
	})
}

func (a authHandler) SignupFacebook(c *gin.Context) {
	formThirdParty := form.ThirdParty{}
	if err := formThirdParty.Bind(c); err != nil {
		panic(err)
	}

	accessToken, err := entity.EntityAuth.SignupFacebook(formThirdParty)
	if err != nil {
		panic(err)
	}

	c.JSON(http.StatusOK, gin.H{
		"accessToken": accessToken,
	})
}

func (a authHandler) LoginFacebook(c *gin.Context) {
	formThirdParty := form.ThirdParty{}
	if err := formThirdParty.Bind(c); err != nil {
		panic(err)
	}

	facebookUser, err := entity.EntityAuth.LoginFacebook(formThirdParty)
	if err != nil {
		panic(err)
	}

	resp := view.PopulateUserFacebook(facebookUser)
	c.JSON(http.StatusOK, gin.H{
		"data": resp,
	})

}

func (a authHandler) SignupApple(c *gin.Context) {
	formThirdParty := form.ThirdParty{}
	if err := formThirdParty.Bind(c); err != nil {
		panic(err)
	}

	accessToken, err := entity.EntityAuth.SignupApple(formThirdParty)
	if err != nil {
		panic(err)
	}

	c.JSON(http.StatusOK, gin.H{
		"accessToken": accessToken,
	})
}

func (a authHandler) SignupGoogle(c *gin.Context) {
	formThirdParty := form.ThirdParty{}
	if err := formThirdParty.Bind(c); err != nil {
		panic(err)
	}

	accessToken, err := entity.EntityAuth.SignupGoogle(formThirdParty)
	if err != nil {
		panic(err)
	}

	c.JSON(http.StatusOK, gin.H{
		"accessToken": accessToken,
	})
}
