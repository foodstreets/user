package handler

import (
	"gitlab.com/foodstreets/user/api/entity"
	"gitlab.com/foodstreets/user/api/view"

	"github.com/gin-gonic/gin"
)

type configHandler struct{}

var ConfigHandler IConfigHandler

type IConfigHandler interface {
	List(c *gin.Context)
}

func init() {
	ConfigHandler = &configHandler{}
}

func (a configHandler) List(c *gin.Context) {
	appVersion, err := entity.AppVersion.GetLastVersion()
	if err != nil {
		return
	}

	quickReviews, err := entity.QuickReview.List()
	if err != nil {
		return
	}

	resp := view.Config.PopulateConfig(appVersion, quickReviews)
	c.JSON(200, gin.H{
		"data": resp,
	})
}
