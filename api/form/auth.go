package form

import (
	"github.com/gin-gonic/gin"
)

type Auth struct {
	AccessToken string `json:"accessToken"`
}

type Login struct {
	PhoneNumber string `json:"phoneNumber"`
	Password    string `json:"password"`
}

type Signup struct {
	PhoneNumber string `json:"phoneNumber"`
	Password    string `json:"password"`
	Platform    string `json:"platform"`
	Device      string `json:"device"`
}

type Verify struct {
	PhoneNumber string `json:"phoneNumber"`
	Session     string `json:"session"`
}

type ThirdParty struct {
	AccessToken string `json:"accessToken" binding:"required"`
	Device      string `json:"device"`
	Platform    string `json:"platform"`
	Email       string `json:"email"`
	Username    string `json:"username"`
	SDKType     string `json:"sdkType"`
}

func (input *Auth) Bind(c *gin.Context) error {
	return c.Bind(&input)
}

func (input *Login) Bind(c *gin.Context) error {
	return c.Bind(&input)
}

func (input *Signup) Bind(c *gin.Context) error {
	return c.Bind(&input)
}

func (input *Verify) Bind(c *gin.Context) error {
	return c.Bind(&input)
}

func (input *ThirdParty) Bind(c *gin.Context) error {
	return c.Bind(&input)
}
