package entity

import (
	"gitlab.com/foodstreets/master/repo"

	"gitlab.com/foodstreets/master/model"
)

type quickReview struct{}

var QuickReview IQuickReview

type IQuickReview interface {
	List() ([]model.QuickReview, error)
}

func init() {
	QuickReview = &quickReview{}
}

func (a quickReview) List() ([]model.QuickReview, error) {
	src := []model.QuickReview{}

	key := model.KeyRedisQuickReview
	err := repo.Redis.Get(key, &src)
	return src, err
}
