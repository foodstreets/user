package entity

import (
	reqAuth "gitlab.com/foodstreets/auth/protobuf/request"
	"gitlab.com/foodstreets/user/api/form"

	respUser "gitlab.com/foodstreets/auth/protobuf/response"

	clientAuth "gitlab.com/foodstreets/auth/init/client"
)

type entityAuth struct{}

var EntityAuth IEntityAuth

type IEntityAuth interface {
	Signup(form form.Signup) (*respUser.User, error)
	Login(form form.Login) (*respUser.User, error)
	GetByAccessToken(form form.Auth) (*respUser.User, error)
	Verify(form form.Verify) (*respUser.User, error)
	SignupFacebook(form form.ThirdParty) (string, error)
	LoginFacebook(form form.ThirdParty) (*respUser.AccountInfo, error)
	SignupApple(form form.ThirdParty) (string, error)
	SignupGoogle(form form.ThirdParty) (string, error)
}

func init() {
	EntityAuth = &entityAuth{}
}

func (e entityAuth) GetByAccessToken(form form.Auth) (resp *respUser.User, err error) {
	req := reqAuth.Auth{
		AccessToken: form.AccessToken,
	}
	auth, err := clientAuth.AuthClient.Authentication(&req)
	if err != nil {
		return
	}

	return auth, nil
}

func (e entityAuth) Signup(form form.Signup) (resp *respUser.User, err error) {
	req := reqAuth.SignupUser{
		PhoneNumber: form.PhoneNumber,
		Password:    form.Password,
		Platform:    form.Platform,
		Device:      form.Device,
	}

	user, err := clientAuth.UserClient.Signup(&req)
	if err != nil {
		return
	}

	return user, nil
}

func (e entityAuth) Login(form form.Login) (resp *respUser.User, err error) {
	req := reqAuth.LoginUser{
		PhoneNumber: form.PhoneNumber,
		Password:    form.Password,
	}

	user, err := clientAuth.UserClient.Login(&req)
	if err != nil {
		return
	}

	return user, nil
}

func (e entityAuth) Verify(form form.Verify) (resp *respUser.User, err error) {
	req := reqAuth.VerifyUser{
		PhoneNumber: form.PhoneNumber,
		Session:     form.Session,
	}

	user, err := clientAuth.UserClient.Verify(&req)
	if err != nil {
		return
	}

	return user, nil
}

func (e entityAuth) SignupFacebook(form form.ThirdParty) (string, error) {
	req := reqAuth.ThirdParty{
		AccessToken: form.AccessToken,
	}

	login, err := clientAuth.UserClient.SignupFacebook(&req)
	if err != nil {
		return "", err
	}

	return login.AccessToken, nil
}

func (e entityAuth) LoginFacebook(form form.ThirdParty) (*respUser.AccountInfo, error) {
	req := reqAuth.ThirdParty{
		AccessToken: form.AccessToken,
	}

	facebookUser, err := clientAuth.UserClient.LoginFacebook(&req)
	return facebookUser, err
}

func (e entityAuth) SignupApple(form form.ThirdParty) (string, error) {
	req := reqAuth.ThirdParty{
		AccessToken: form.AccessToken,
	}

	login, err := clientAuth.UserClient.SignupApple(&req)
	if err != nil {
		return "", err
	}

	return login.AccessToken, nil
}

func (e entityAuth) SignupGoogle(form form.ThirdParty) (string, error) {
	req := reqAuth.ThirdParty{
		AccessToken: form.AccessToken,
	}

	login, err := clientAuth.UserClient.SignupGoogle(&req)
	if err != nil {
		return "", err
	}

	return login.AccessToken, nil
}
