package entity

import (
	"gitlab.com/foodstreets/master/repo"

	"gitlab.com/foodstreets/master/model"
)

type appVersion struct{}

var AppVersion IAppVersion

type IAppVersion interface {
	GetLastVersion() (model.AppVersion, error)
}

func init() {
	AppVersion = &appVersion{}
}

func (a appVersion) GetLastVersion() (model.AppVersion, error) {
	src := model.AppVersion{}

	key := model.KeyRedisAppVersion
	err := repo.Redis.Get(key, &src)
	return src, err
}
