package view

import (
	"gitlab.com/foodstreets/master/model"
)

type appVersion struct{}

var AppVersion appVersion

type AppVersionView struct {
	Id          int    `json:"id"`
	Version     string `json:"version"`
	Type        string `json:"type"`
	Title       string `json:"title"`
	Message     string `json:"message"`
	BuildNumber int    `json:"build_number"`
}

func (a appVersion) DefaultAppVersion(appVersion model.AppVersion) AppVersionView {
	return AppVersionView{
		Id:          appVersion.Id,
		Version:     appVersion.Version,
		Type:        appVersion.Type,
		Title:       appVersion.Title,
		Message:     appVersion.Message,
		BuildNumber: appVersion.BuildNumber,
	}
}
