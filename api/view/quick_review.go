package view

import (
	"gitlab.com/foodstreets/master/model"
)

type quickReview struct{}

var QuickReview quickReview

type QuickReviewView struct {
	Id       int    `json:"id"`
	TypeName string `json:"type_name"`
	Image    string `json:"image"`
}

func (a quickReview) DefaultQuickReview(quickReview model.QuickReview) QuickReviewView {
	return QuickReviewView{
		Id:       quickReview.Id,
		TypeName: quickReview.TypeName,
		Image:    quickReview.Image,
	}
}

func (a quickReview) PopulateQuickReviews(quickReviews []model.QuickReview) []QuickReviewView {
	if len(quickReviews) == 0 {
		return []QuickReviewView{}
	}

	views := make([]QuickReviewView, len(quickReviews))
	for i, quickReview := range quickReviews {
		views[i] = a.DefaultQuickReview(quickReview)
	}
	return views
}
