package view

import (
	"gitlab.com/foodstreets/master/model"
)

type config struct{}

var Config config

type ConfigView struct {
	AppVersion  AppVersionView    `json:"app_version"`
	QuickReview []QuickReviewView `json:"quick_review"`
}

func (config) PopulateConfig(appVersion model.AppVersion, quickReviews []model.QuickReview) ConfigView {
	return ConfigView{
		AppVersion:  AppVersion.DefaultAppVersion(appVersion),
		QuickReview: QuickReview.PopulateQuickReviews(quickReviews),
	}
}
