package view

import (
	"time"

	respUser "gitlab.com/foodstreets/auth/protobuf/response"
)

type User struct {
	Id        int        `json:"id"`
	Session   string     `json:"session"`
	CreatedAt *time.Time `json:"createdAt"`
	UpdatedAt *time.Time `json:"updatedAt"`
	CreatedBy int        `json:"createdBy"`
	UpdatedBy int        `json:"updatedBy"`
}

type AccountInfo struct {
	Username string `json:"username"`
	Avatar   string `json:"avatar"`
}

func PopulateUser(user *respUser.User) User {
	if user != nil {
		return User{}
	}
	createdAt := time.Unix(user.CreatedAt, 0)
	updatedAt := time.Unix(user.UpdatedAt, 0)
	return User{
		Id:        int(user.Id),
		Session:   user.Session,
		CreatedAt: &createdAt,
		UpdatedAt: &updatedAt,
		CreatedBy: int(user.CreatedBy),
		UpdatedBy: int(user.UpdatedBy),
	}
}

func PopulateUserFacebook(user *respUser.AccountInfo) AccountInfo {
	if user != nil {
		return AccountInfo{}
	}
	return AccountInfo{
		Username: user.Username,
		Avatar:   user.Avatar,
	}
}
