package config

import (
	"sync"

	"gitlab.com/foodstreets/master/config"
)

var (
	once sync.Once
	conf Config
)

type Config struct {
	config.Config
}

func load() {
	once.Do(func() {
		conf.Config = config.Load()
	})
}

func Get() Config {
	load()
	return conf
}
