FROM golang:1.16

RUN mkdir -p /go/src

COPY . /go/src

ARG USER_KEY
ARG DEPLOY_KEY

RUN echo "${USER_KEY}"
RUN echo "${DEPLOY_KEY}"

RUN git config --global url."https://${USER_KEY}:${DEPLOY_KEY}@gitlab.com".insteadOf "https://gitlab.com"

WORKDIR /go/src
RUN go mod download
#
RUN go build -o /fs-users
#
EXPOSE 8081
#
CMD [ "/fs-users" ]
