package middleware

import (
	"strconv"

	"gitlab.com/foodstreets/auth/protobuf/request"

	"gitlab.com/foodstreets/auth/protobuf/response"

	authClient "gitlab.com/foodstreets/auth/init/client"

	mMiddleware "gitlab.com/foodstreets/master/middleware"

	"github.com/gin-gonic/gin"
)

type auth struct {
	currentUserFunc mMiddleware.GetCurrentUser
}

var Auth auth

func InitAuth(f mMiddleware.GetCurrentUser) {
	Auth.currentUserFunc = f
}

func (a auth) GetCurrentUser(c *gin.Context) *response.User {
	user, exists := a.currentUserFunc(c)
	if !exists {
		return nil
	}
	resp := user.(response.User)
	return &resp

}

func GetCurrentUser(idStr string) (interface{}, error) {
	id, err := strconv.Atoi(idStr)
	if err != nil {
		return nil, err
	}

	req := request.User{
		Id: int64(id),
	}
	user, err := authClient.UserClient.GetByID(&req)
	if err != nil {
		return nil, err
	}
	return user, nil
}
