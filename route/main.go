package route

import (
	"net/http"

	"gitlab.com/foodstreets/user/api/handler"
	"gitlab.com/foodstreets/user/middleware"

	mMiddleware "gitlab.com/foodstreets/master/middleware"

	"github.com/gin-gonic/gin"
)

var (
	version = "version"
)

func Init() *gin.Engine {

	r := gin.Default()
	r.Use(gin.Recovery())
	r.Use(mMiddleware.CORS())

	hashKey := "10116c519f514c8a68f451bbbde6efedb2f3b1b18a368ffcdb4da4aa7e57baf7"
	blockKey := "d9b0fddec1df90c5d29bfe56e0f375d0"
	securecCookie := mMiddleware.NewSecureCookie(blockKey, hashKey)
	authenMiddleware := mMiddleware.NewAuthenMiddleware(securecCookie, middleware.GetCurrentUser)
	middleware.InitAuth(authenMiddleware.GetCurrentUser)

	handlerAuth := handler.AuthHandler
	handlerConfig := handler.ConfigHandler

	groupVersion := r.Group(":" + version)
	groupVersion.Use(authenMiddleware.Interception())

	authenticate := groupVersion.Group("/authenticate")
	{
		POST(authenticate, "", handlerAuth.GetByAccessToken)
		POST(authenticate, "/login", handlerAuth.Login)

		// PhoneNumber
		POST(authenticate, "/signup", handlerAuth.Signup)
		POST(authenticate, "/signup/verify", handlerAuth.Verify)

		// Facebook
		POST(authenticate, "/facebook", handlerAuth.LoginFacebook)
		POST(authenticate, "/facebook/signup", handlerAuth.SignupFacebook)

		// Apple
		POST(authenticate, "/apple/signup", handlerAuth.SignupApple)

		// Google
		POST(authenticate, "google/signup", handlerAuth.SignupGoogle)

	}

	config := groupVersion.Group("/configs")
	{
		GET(config, "", handlerConfig.List)
	}

	return r
}

// Override Method
func POST(group *gin.RouterGroup, relativePath string, handlers func(*gin.Context)) {
	route(group, http.MethodPost, relativePath, handlers)
}
func GET(group *gin.RouterGroup, relativePath string, handlers func(*gin.Context)) {
	route(group, http.MethodGet, relativePath, handlers)
}
func DELETE(group *gin.RouterGroup, relativePath string, handlers func(*gin.Context)) {
	route(group, http.MethodDelete, relativePath, handlers)
}
func PUT(group *gin.RouterGroup, relativePath string, handlers func(*gin.Context)) {
	route(group, http.MethodPut, relativePath, handlers)
}

func route(group *gin.RouterGroup, method, relativePath string, handlers func(*gin.Context)) {
	switch method {
	case http.MethodPost:
		group.POST(relativePath, handlers)
	case http.MethodGet:
		group.GET(relativePath, handlers)
	case http.MethodDelete:
		group.DELETE(relativePath, handlers)
	case http.MethodPut:
		group.PUT(relativePath, handlers)
	}
}
